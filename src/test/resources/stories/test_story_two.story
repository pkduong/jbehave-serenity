Scenario: Story Two
Given This is test story two

Scenario: Test-2 This is step 1
Given I want to test step 2.1
Then Check assertTrue value: true

Scenario: Test-2 This is step 2
Given I want to test step 2.2
Then Check assert to collector value: false

Scenario: Test-2 This is step 3
Given I want to test step 2.3
Then Check assertTrue value: true

Scenario: Test-2 This is step 4
Given I want to test step 2.4
Then Check assertThat value: true
