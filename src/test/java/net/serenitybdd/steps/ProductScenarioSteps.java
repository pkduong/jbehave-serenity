package net.serenitybdd.steps;

import static org.junit.Assert.assertTrue;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import net.serenitybdd.pages.SearchPage;


public class ProductScenarioSteps {

	SearchPage searchPage;
	
	public static String PRODUCT_QUERY="";
	
	@When("I want to search for product: $product")
	public void enterSearchProductQueryString(String product){
		PRODUCT_QUERY = product;
		System.out.println("[enterSearchProductQueryString] PRODUCT_QUERY: " + PRODUCT_QUERY);
		searchPage.enterSearchQuery(PRODUCT_QUERY);
		searchPage.clickButtonSearch();
	}
	
	
	@Then("I should see list of products return contains the product above")
	public void checkFirstItemInTheListContainsTheProduct(){
		searchPage.checkSearchResultContainsQuery(PRODUCT_QUERY);
	}
	
	
	@When("I click on the top item")
	public void clickOnFirstItem(){
		searchPage.clickOnTheFirstItem();
	}
	
	@Then("I see product detail page display")
	public void checkProductDetailPageDisplay(){
		assertTrue(true);
	}
	


}
