package net.serenitybdd.steps.serenity;

import net.serenitybdd.BaseSs;
import net.serenitybdd.pages.HomePage;
import net.serenitybdd.pages.LoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class LoginSteps extends ScenarioSteps {

	
	private static final long serialVersionUID = 1L;
	LoginPage loginPage;
	HomePage homePage;

	@Step
	public void opens_home_page(String username, String password) {
		
		loginPage.open();
	}
	
	@Step
	public void opens_login_or_home_page(String username, String password) {
		
		System.out.println("<opens_login_or_home_page> IS_ALREADY_LOGIN: " + BaseSs.IS_ALREADY_LOGIN);
		
		if (BaseSs.IS_ALREADY_LOGIN==false){
			
			loginPage.open();
			loginPage.enterUserName(username);
			loginPage.enterPassword(password);
			loginPage.login();
			
			BaseSs.IS_ALREADY_LOGIN=true;
			
		}else{
			loginPage.open();
			homePage.clickOnEtsyLogo();
		}
	}

	@Step
	public void inputUserName(String username) {
		loginPage.enterUserName(username);
	}

	@Step
	public void inputPassword(String password) {
		loginPage.enterPassword(password);
	}

	@Step
	public void clickLoginButton() {
		loginPage.login();
	}


	
}
