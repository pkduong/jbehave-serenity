package net.serenitybdd.steps;


import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.junit.Rule;
import org.junit.rules.ErrorCollector;

import static org.junit.Assert.assertTrue;

import static org.assertj.core.api.Assertions.assertThat;

public class SystemSteps {

    @Rule
    public ErrorCollector collector = new ErrorCollector();

    @Given("I want to test debug")
    public void test_story_debug(){
        System.out.println("<test_story_debug>");
        boolean result = false;

//        try {
//            assertTrue(result);
//        }catch (Exception e){
//            System.out.println("EXEPTION: " + e.getMessage());
//        }

        //assertTrue(result);
        assertThat(result).isTrue();

//        collector.checkThat("a", equalTo("b"));
//        collector.checkThat(1, equalTo(2));

    }

    @Given("This is test story two")
    public void test_two(){
        System.out.println("<test_story_two>");
        boolean result = true;

        assertTrue(result);
    }

    @Given("I want to test step $stepId")
    public void step_define(String stepId){
        System.out.println("- Story Step: " + stepId);
    }

    @Then("Check assertTrue value: $value")
    public void step_define_check_assertTrue(String value){
        boolean result = false;

        result = value.contains("true");

        System.out.println("- assertTrue("+value+")");
        assertTrue(result);
    }

    @Then("Check assertThat value: $value")
    public void step_define_check_assertThat(String value){
        boolean result = false;

        result = value.contains("true");

        System.out.println("- assertThat("+value+")");
        assertThat(result).isTrue();
    }

    @Then("Check assert to collector value: $value")
    public void step_define_check_collector(String value){

        boolean result = false;
        result = value.contains("true");
        System.out.println("- assertTrue("+value+")" + "throw error to collector");

        try {
            assertTrue(result);
        }catch (Throwable t) {
            collector.addError(t);
        }
    }

}
