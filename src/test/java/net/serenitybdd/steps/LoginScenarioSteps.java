package net.serenitybdd.steps;

import java.text.ParseException;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import net.serenitybdd.pages.HomePage;
import net.serenitybdd.steps.serenity.LoginSteps;
import net.thucydides.core.annotations.Steps;

import static org.junit.Assert.assertTrue;

public class LoginScenarioSteps {
	@Steps
	LoginSteps login;
	
	
	HomePage hp;

	@When("I want to login with $username and $password")
	public void login(String username, String password) {
		login.opens_home_page(username, password);
		login.inputUserName(username);
		login.inputPassword(password);
		login.clickLoginButton();
	}

	@Then("I should see etsy homepage")
	public void resultsForInvalidLogin() {
		assertTrue(hp.isUserIconVisible());
	}
	
	@Given("Pass login page")
	public void opens_login_or_home_page(){
		login.opens_login_or_home_page("duongpham1", "duongthu");
	}

	
}
