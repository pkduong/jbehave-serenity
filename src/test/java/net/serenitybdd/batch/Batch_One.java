package net.serenitybdd.batch;

import net.serenitybdd.acceptanceTest.SearchProduct;
import net.serenitybdd.acceptanceTest.ViewProductDetail;
import net.serenitybdd.implement.Test_Story;
import net.serenitybdd.implement.Test_Story_Three;
import net.serenitybdd.implement.Test_Story_Two;
import net.serenitybdd.jbehave.SerenityStory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        Test_Story.class,
        Test_Story_Two.class,
        Test_Story_Three.class
})

public class Batch_One extends SerenityStory {
}
