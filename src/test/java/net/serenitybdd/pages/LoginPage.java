package net.serenitybdd.pages;

import com.google.common.base.*;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@DefaultUrl("https://www.etsy.com/signin?from_page=https%3A%2F%2Fwww.etsy.com%2F")
public class LoginPage extends PageObject {

	@FindBy(id = "signin_button")
	WebElementFacade loginButton;
	@FindBy(xpath = "//input[@name='username']")
	WebElementFacade usernameInput;
	@FindBy(xpath = "//input[@name='password']")
	WebElementFacade passwordInput;

	public void enterUserName(String username) {
		usernameInput.type(username);
	}

	public void enterPassword(String password) {
		passwordInput.type(password);
	}

	private void waitForElementToBeUpdatedTo(WebElementFacade element, String value) {
		waitForCondition().withTimeout(5, TimeUnit.SECONDS).pollingEvery(250, TimeUnit.MILLISECONDS)
				.until(FieldIsUpdatedTo(element, value));
	}

	private Function<? super WebDriver, Boolean> FieldIsUpdatedTo(WebElementFacade element, String value) {
		return new Function<WebDriver, Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				return element.getValue().equalsIgnoreCase(value);
			}
		};
	}

	public void login() {
		loginButton.click();
	}

	
}
