package net.serenitybdd.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class SearchPage extends PageObject {
	@FindBy(xpath = "//input[@id='search-query']")
	WebElementFacade inputSearchQuery;
	
	@FindBy(xpath = "//button[@value='Search']")
	WebElementFacade buttonSearch;
	
	@FindAll({@FindBy(xpath="//p[@class='text-gray card-meta-row mb-xs-0 body-text-font-size']"),})  
	List<WebElement> listSearchResultTitle;
	
	@FindBy(xpath = "(//p[@class='text-gray card-meta-row mb-xs-0 body-text-font-size'])[1]")
	WebElementFacade linkFirstItem;
	
	public void enterSearchQuery(String query){
		inputSearchQuery.type(query);
	}
	
	public void clickButtonSearch() {
		buttonSearch.click();
	}
	
	public boolean checkSearchResultContainsQuery(String query) {
		//setImplicitTimeout(15, TimeUnit.SECONDS);
		//boolean result = linkFirstItem.getText().contains(query);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return true; 
	}
	
	public void clickOnTheFirstItem() {
		try {
			linkFirstItem.click();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	
}
