package net.serenitybdd.pages;

import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://www.etsy.com/?show_panel=true")
public class HomePage extends PageObject{

	@FindBy(xpath = "//a[@href='https://www.etsy.com/people/duongpham1?ref=hdr']")
	WebElementFacade userIcon;
	
	@FindBy(xpath = "//a[@aria-label='Etsy Logo']")
	WebElementFacade etsyLogo;
	
	public boolean isUserIconVisible(){
		return userIcon.isCurrentlyVisible();
	}
	
	public void clickOnEtsyLogo(){
		etsyLogo.click();
	}
	
}
