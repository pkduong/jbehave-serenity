package net.serenitybdd;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;


@Configuration
@ComponentScan("net.serenitybdd")
public class IntegrationTestContext {

    @Autowired
    private EnvironmentVariables environmentVariables;

    @Bean
    public EnvironmentVariables getSystemEnvironmentVariables() {
        return SystemEnvironmentVariables.createEnvironmentVariables();
    }

    
}
